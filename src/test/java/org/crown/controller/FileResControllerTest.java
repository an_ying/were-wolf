package org.crown.controller;/**
 * Created by kete on 2019-04-11.
 */

import org.apache.commons.io.FilenameUtils;
import org.crown.framework.SuperRestControllerTest;
import org.crown.framework.test.ControllerTest;
import org.crown.mapper.FileMapper;
import org.crown.model.dto.TokenDTO;
import org.crown.model.entity.File;
import org.crown.model.entity.Role;
import org.crown.model.parm.RolePARM;
import org.crown.service.IFileService;
import org.crown.service.IRoleService;
import org.crown.service.IUserService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * 作者：kete
 * 创建时间：2019-04-11 15:14
 * 功能描述：
 * 版本：
 */
public class FileResControllerTest extends SuperRestControllerTest implements ControllerTest {

    @Autowired
    private IUserService userService;

    @Autowired
    private IFileService fileService;

    @Autowired
    private IRoleService iRoleService;

    @Autowired
    private RoleRestController restController;


    private TokenDTO token;

    private MockMvc mockMvc;

    @Before
    @Override
    public void before() {

        mockMvc = getMockMvc(restController);
        token = userService.getToken(userService.getById(1));
    }

    @Test
    public void test2() throws Exception{
        RolePARM parm = new RolePARM();
        parm.setRoleName("测试管理员");
        parm.setRemark("aaaa");

        Role role = parm.convert(Role.class);
        role.setCreateUid(1);
        role.setCreateTime(LocalDateTime.now());
        role.setUpdateUid(1);
        role.setUpdateTime(LocalDateTime.now());
        iRoleService.save(role);
    }

    @Test
    public void test1(){

        File sysFile = new File();
        sysFile.setFileName("test.text");
        sysFile.setFileAddress("d:/test.text");
        sysFile.setFileSize(6340L);
        sysFile.setFileType("text");

        fileService.save(sysFile);

        int fileId = sysFile.getId();

        System.out.println(fileId);

        File aa = fileService.getById(fileId);

        System.out.println(aa.toString());
    }


}
