/*
 * Copyright (c) 2018-2022 Caratacus, (caratacus@qq.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.crown.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.crown.common.annotations.Resources;
import org.crown.enums.AuthTypeEnum;
import org.crown.enums.StatusEnum;
import org.crown.framework.responses.ApiResponses;
import org.crown.model.entity.Content;
import org.crown.service.IContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;

import io.swagger.annotations.Api;
import org.crown.framework.controller.SuperController;

/**
 * <p>
 * 内容表 前端控制器
 * </p>
 *
 * @author Caratacus
 */
@Api(tags = {"Content"}, description = "内容表相关接口")
@RestController
@RequestMapping(value = "/content", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Validated
public class ContentRestController extends SuperController {
    @Autowired
    public IContentService contentService;

    @Resources
    @ApiOperation(value = "查询类容列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "title", value = "标题", paramType = "query")
    })
    @GetMapping
    public ApiResponses<IPage<Content>> page(@RequestParam(value = "title", required = false) String title){
        return success(contentService.query()
        .like(StringUtils.isNotEmpty(title),Content::getTitle,title)
        .page(this.getPage()));
    }

    @Resources(auth = AuthTypeEnum.OPEN)
    @ApiOperation(value = "查看内容")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "内容ID", required = true, paramType = "path")
    })
    @GetMapping("/{id}")
    public ApiResponses<Content> get(@PathVariable("id") Integer id) {
        return success(contentService.getById(id));
    }


    @Resources(auth = AuthTypeEnum.OPEN)
    @ApiOperation(value = "添加内容")
    @PostMapping
    public ApiResponses<Void> create(@RequestBody Content content){

        content.setStatus(StatusEnum.DISABLE.getValue());
        contentService.save(content);

        return success(HttpStatus.CREATED);
    }
}
