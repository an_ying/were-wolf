/*
 * Copyright (c) 2018-2022 Caratacus, (caratacus@qq.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.crown.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.common.base.Charsets;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.crown.common.annotations.Resources;
import org.crown.common.utils.JacksonUtils;
import org.crown.enums.AuthTypeEnum;
import org.crown.framework.responses.ApiResponses;
import org.crown.model.dto.JsonDTO;
import org.crown.model.entity.File;
import org.crown.service.IFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;

import io.swagger.annotations.Api;
import org.crown.framework.controller.SuperController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author Caratacus
 */
@Api(tags = {"File"}, description = "相关接口")
@RestController
@RequestMapping(value = "/file", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Validated
public class FileRestController extends SuperController {

    private final static Logger logger = LoggerFactory.getLogger(FileRestController.class);

    private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;

    @Autowired
    private IFileService service;

    @Resources
    @ApiOperation(value = "查询文件列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fileName", value = "文件名称", paramType = "query")
    })
    @GetMapping
    public ApiResponses<IPage<File>> page(@RequestParam(value = "fileName", required = false) String fileName) {
        return success(service.query()
                .like(StringUtils.isNotEmpty(fileName), File::getFileName, fileName)
                .page(this.getPage()));
    }

    @Resources
    @ApiOperation(value = "文件上传")
    @PostMapping("/upload")
    public ApiResponses<JsonDTO> upload(@RequestParam(value = "file", required = false) MultipartFile file,String fileName){


        logger.info("file {}", JacksonUtils.toJson(file));
        int fileId = service.putFile(file,fileName);

        JsonDTO dto = new JsonDTO();
        if(fileId>0){
            dto.setCode(0);
            dto.put("fileId",fileId);
        }else{
            dto.setCode(-1);
        }
        return success(dto);

    }

    @Resources(auth = AuthTypeEnum.OPEN)
    @ApiOperation(value = "文件下载")
    @GetMapping("/download/{id}")
    public void download(@PathVariable("id")  String fileId){

        File sysDocFile = service.getById(fileId);

        if(null==sysDocFile){
            return;
        }

        /** 要下载的文件 */
        java.io.File downloadFile = new java.io.File(sysDocFile.getFileAddress());


        /** 记录文件大小 */
        long fileLength = downloadFile.length();

        /** 记录已下载文件大小 */
        long pastLength = 0;

        /** 0:从头开始的全文下载;1:从某字节开始的下载（bytes=8192-）;2:从某字节开始到某字节结束的下载（bytes=8192-1048576） */
        int rangeSwitch = 0;

        /** 记录客户端需要下载的字节段的最后一个字节偏移量(比如bytes=8192-1048576,则这个值是为1048576) */
        long toLength = 0;

        /** 客户端请求的字节总量 */
        long contentLength = 0;

        /** 记录客户端传来的形如"bytes=8192-"或者"bytes=8192-1048576"的内容 */
        String rangeBytes = "";//

        /** 随机读取数据 */
        RandomAccessFile raf = null;

        /** 写出数据*/
        OutputStream os = null;

        /** 缓冲*/
        OutputStream out = null;

        /** 暂存容器 */
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];


        /**  客户端请求的下载的文件块的开始字节 */
        if (request.getHeader("Range") != null) {
            response.setStatus(javax.servlet.http.HttpServletResponse.SC_PARTIAL_CONTENT);
            logger.info("request.getHeader(\"Range\")=" + request.getHeader("Range"));
            rangeBytes = request.getHeader("Range").replaceAll("bytes=", "");


            if (rangeBytes.indexOf('-') == rangeBytes.length() - 1) {/** bytes=8192- */
                rangeSwitch = 1;
                rangeBytes = rangeBytes.substring(0, rangeBytes.indexOf('-'));
                pastLength = Long.parseLong(rangeBytes.trim());

                /**客户端请求的是 8192 之后的字节 */
                contentLength = fileLength - pastLength + 1;
            } else {/** bytes=8192-1048576 */
                rangeSwitch = 2;
                String temp0 = rangeBytes.substring(0,rangeBytes.indexOf('-'));
                String temp2 = rangeBytes.substring(rangeBytes.indexOf('-') + 1, rangeBytes.length());

                /** bytes=8192-1048576,从第 8192 个字节开始下载*/
                pastLength = Long.parseLong(temp0.trim());

                /** bytes=8192-1048576,到第 1048576 个字节结束*/
                toLength = Long.parseLong(temp2);

                /**客户端请求的是 8192-1048576  之间的字节 */
                contentLength = toLength - pastLength + 1;//
            }
        } else {/** 从开始进行下载 */
            /** 客户端要求全文下载*/
            contentLength = fileLength;
        }

        /**
         * 如果设设置了Content-Length，则客户端会自动进行多线程下载。如果不希望支持多线程，则不要设置这个参数。
         * 响应的格式是:
         * Content-Length: [文件的总大小] - [客户端请求的下载的文件块的开始字节]
         * response().setHeader("Content-Length",new Long(file.length() - p).toString());
         */
        /** 告诉客户端允许断点续传多线程连接下载,响应的格式是:Accept-Ranges: bytes */
        response.reset();

        /** 如果是第一次下,还没有断点续传,状态是默认的 200,无需显式设置;响应的格式是:HTTP/1.1 200 OK */
        response.setHeader("Accept-Ranges", "bytes");
        if (pastLength != 0) {
            /**不是从最开始下载,  响应的格式是:
             * Content-Range: bytes [文件块的开始字节]-[文件的总大小 - 1]/[文件的总大小]
             */
            logger.info("不是从开始进行下载！服务器即将开始断点续传...");
            switch (rangeSwitch) {
                case 1 : {/** 针对 bytes=8192- 的请求 */
                    String contentRange = new StringBuffer("bytes ").append(Long.toString(pastLength)).append("-").append(Long.toString(fileLength - 1)).append("/").append(Long.toString(fileLength)).toString();
                    response.setHeader("Content-Range", contentRange);
                    break;
                } case 2 : {/** 针对 bytes=8192-1048576 的请求 */
                    String contentRange = rangeBytes + "/" + Long.toString(fileLength);
                    response.setHeader("Content-Range", contentRange);
                    break;
                } default : {
                    break;
                }
            }
        } else {
            /** 是从开始下载 */
            logger.info("是从0开始进行下载！");
        }

        try {
            setResponse(sysDocFile.getFileName(),request,response);
            response.addHeader("Content-Length", String.valueOf(contentLength));
            os = response.getOutputStream();
            out = new BufferedOutputStream(os);
            raf = new RandomAccessFile(downloadFile, "r");
            try {
                switch (rangeSwitch) {
                    case 0 : {/** 普通下载，或者从头开始的下载 */
                    } case 1 : {/** 针对 bytes=8192- 的请求 */
                        raf.seek(pastLength);/** 形如 bytes=8192- 的客户端请求，跳过 8192  个字节 */
                        int n = 0;
                        while ((n = raf.read(buffer, 0, 1024)) != -1) {
                            out.write(buffer, 0, n);
                        }
                        break;
                    }
                    case 2 : {/** 针对 bytes=8192-1048576 的请求 */
                        raf.seek(pastLength - 1);/** 形如 bytes=8192-1048576 的客户端请求，找到第 1048576 个字节 */
                        int n = 0;

                        /** 记录已读字节数 */
                        long readLength = 0;
                        while (readLength <= contentLength - DEFAULT_BUFFER_SIZE) {/** 大部分字节在这里读取 */
                            n = raf.read(buffer, 0, DEFAULT_BUFFER_SIZE);
                            readLength += DEFAULT_BUFFER_SIZE;
                            out.write(buffer, 0, n);
                        }
                        if (readLength <= contentLength) {/** 余下的不足 1024 个字节在这里读取 */
                            n = raf.read(buffer, 0, (int)(contentLength - readLength));
                            out.write(buffer, 0, n);
                        }

                        break;
                    }
                    default : {
                        break;
                    }
                }
                out.flush();
            } catch(IOException ie) {
                /**
                 * 在写数据的时候，
                 * 对于 ClientAbortException 之类的异常，
                 * 是因为客户端取消了下载，而服务器端继续向浏览器写入数据时，
                 * 抛出这个异常，这个是正常的。
                 * 尤其是对于迅雷这种吸血的客户端软件，
                 * 明明已经有一个线程在读取 bytes=1275856879-1275877358，
                 * 如果短时间内没有读取完毕，迅雷会再启第二个、第三个。。。线程来读取相同的字节段，
                 * 直到有一个线程读取完毕，迅雷会 KILL 掉其他正在下载同一字节段的线程，
                 * 强行中止字节读出，造成服务器抛 ClientAbortException。
                 * 所以，我们忽略这种异常
                 */
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            IOUtils.closeQuietly(out);
            if (raf != null) {
                try {
                    raf.close();
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
    }

    public void setResponse(String fileName, HttpServletRequest request, HttpServletResponse response) {

        try {
            String agent = request.getHeader("USER-AGENT");

            String contentType = null;
            String attachment = null;
            String ext = FilenameUtils.getExtension(fileName);

            if(StringUtils.equalsIgnoreCase("swf", ext)) {
                contentType = "application/x-shockwave-flash";
                attachment = "application/x-shockwave-flash";
            } else {
                contentType = "application/octet-stream";
                attachment = "aattachment";
            }

            if (null != agent && (-1 != agent.indexOf("MSIE") || -1 != agent.indexOf("Trident")))  {
                fileName = URLEncoder.encode(fileName, "utf-8");
            } else {
                fileName = new String(fileName.getBytes(Charsets.UTF_8), Charsets.ISO_8859_1);
            }

            response.setContentType(contentType);
            response.addHeader("Content-Disposition",attachment + ";filename=" + makeValidFileName(fileName));
        } catch(Exception e) {
          e.printStackTrace();
        }
    }

    public static String makeValidFileName(String str) throws PatternSyntaxException {

        //清除掉所有特殊字符
        String regEx="[`~!@#$%^&*()+=|{}':;',\\[\\]<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        return m.replaceAll("").trim();
    }

}
