package org.crown.model.dto;/**
 * Created by kete on 2019-04-10.
 */

import java.util.HashMap;

/**
 * 作者：kete
 * 创建时间：2019-04-10 17:07
 * 功能描述：
 * 版本：
 */
public class JsonDTO extends HashMap {
    private Integer code;
    private String success = "1";
    private String message;

    private Integer errCode;
    private String errMsg;


    public JsonDTO() {
        setSuccess("1");
        setMessage("");
    }

    public JsonDTO(Integer errCode) {
        if(errCode == null)
            errCode = 0;
        setErrCode(errCode);
        setErrMsg("ok");
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        put("code", code);
        this.code = code;
    }

    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        put("errCode", errCode);
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        put("errMsg", errMsg);
        this.errMsg = errMsg;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        put("success", success);
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        put("message", message);
        this.message = message;
    }

    public void put(String key,Object value) {
        super.put(key, value);
    }
}
