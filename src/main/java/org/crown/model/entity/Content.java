/*
 * Copyright (c) 2018-2022 Caratacus, (caratacus@qq.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.crown.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import org.crown.framework.model.BaseModel;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.crown.framework.model.convert.Convert;


/**
 * <p>
 * 内容表
 * </p>
 *
 * @author Caratacus
 */
@TableName("sys_content")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Content extends Convert {

    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private String id;

    @ApiModelProperty(notes = "标题")
    private String title;
    @ApiModelProperty(notes = "内容")
    private String content;
    @ApiModelProperty(notes = "标签")
    private String tag;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(notes = "创建者ID")
    private Integer createUid;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(notes = "修改者ID")
    private Integer updateUid;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(notes = "创建时间")
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(notes = "修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(notes = "状态 0：禁用 1：正常")
    private Integer status;

}
