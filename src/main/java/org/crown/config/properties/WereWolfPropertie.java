package org.crown.config.properties;/**
 * Created by kete on 2019-05-17.
 */

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 作者：kete
 * 创建时间：2019-05-17 16:55
 * 功能描述：
 * 版本：
 */
@Component
@Data
@ConfigurationProperties(prefix = WereWolfPropertie.PREFIX)
public class WereWolfPropertie {

    public static final String PREFIX = "wolf";

    private String fileUploadPath = "";


}
